from typing import Dict, List, Set, Tuple

from constants import Constants
from game import Game, Player
from intelligence import Intelligence
from strategist import StrategiesUnits, StrategiesCitytiles
from errors import NoCityTilesFoundError


class Commander:

    def __init__(self):
        self.memory: List[Dict[str, any]] = []
        self.assignments: List[Dict[str, any]] = []  # TODO assign units to cities / resource patches
        self.units_with_commands: Set[str] = set()
        self.commands: List[str] = []
        self.intel = Intelligence()

    def command(self, game: Game, player: Player, strategy_citytiles: StrategiesCitytiles,
                strategy_units: StrategiesUnits) -> Tuple[List[str], List[str]]:
        """
        Briefs the Intelligence and uses the provided strategy to decide for each of the units and
        citytiles what action to undertake
        """
        self.commands = []
        self.intel.brief(game, player)
        self.units_with_commands: Set[str] = set()

        citytile_actions = self.command_citytiles(strategy=strategy_citytiles)
        self.command_units(strategy=strategy_units)
        return citytile_actions, self.commands

    def command_citytiles(self, strategy: StrategiesCitytiles) -> List[str]:
        actions: List[str] = []
        units_to_build = 0
        if strategy == StrategiesCitytiles.GROW:
            # Currently we maximally build 1 unit every step
            units_to_build = 1

        # Define the actions to perform per citytile
        for tile in self.intel.get_citytiles(only_can_act=True):
            if units_to_build > 0:
                actions.append(tile.build_worker())
                units_to_build -= 1
            else:
                actions.append(tile.research())
        return actions

    def shout(self, id: str, command: str):
        """
        The shout command will validate if this unit (defined by the id) already received a command.
        If that is the case, the commander will not shout again.
        """
        if id in self.units_with_commands:
            return False

        self.units_with_commands.add(id)
        self.commands.append(command)
        return True

    def command_units(self, strategy: StrategiesUnits):

        if strategy == StrategiesUnits.LOCKDOWN:
            self.command_units_lockdown()
        if strategy == StrategiesUnits.GATHER:
            self.command_units_gather()
        if strategy == StrategiesUnits.BUILD:
            self.command_units_build()
            self.command_units_gather()
        if strategy == StrategiesUnits.EXPAND:
            self.command_units_expand()

    def command_units_lockdown(self):
        """
        Units that are located within a city, they stay put
        Units that are at a resource and have a cargo that is completely full and are within
            walking distance of a neighboring city, they move towards that city
        Units that are at a resource but are not within the close surrounding of a city, they stay
            at the current resource
        Units that are roaming around freely, they move towards the closest city
            TODO - Can be improved by move to a resource if that is significantly closer
        """
        for unit in self.intel.get_workers(only_can_act=True):
            if unit.pos in self.intel.map.city_tiles_positions[self.intel.player.team]:
                # Whenever the unit is in a city, just stay there
                # TODO - Units can move within a city during the night to have a head start the following day
                continue

            if unit.get_cargo_space_left() > 10 and unit.pos in self.intel.map.resource_tiles_positions:
                # Whenever the unit is located on a resource and has got cargo space left, just stay
                # at the resource
                continue

            # Whenever the unit is roaming freely, we move it to the closest citytile unless this one is far away
            # and there is a resource tile located very close to the unit
            distance_to_closest_citytile = self.intel.calculate_shortest_distance_to(from_=[unit.pos], to=self.intel.map.city_tiles_positions[self.intel.player.team])
            if distance_to_closest_citytile is not False:
                position_closest_citytile, distance_to_closest_citytile = distance_to_closest_citytile[0]
            distance_to_closest_resource = self.intel.calculate_shortest_distance_to(from_=[unit.pos], to=self.intel.map.resource_tiles_positions)
            if distance_to_closest_resource is not False:
                position_closest_resource, distance_to_closest_resource = distance_to_closest_resource[0]

            if distance_to_closest_citytile and distance_to_closest_citytile < 6:
                self.shout(unit.id,
                           unit.move(self.intel.define_route(from_=unit.pos, to=position_closest_citytile, avoid_cities=False)))
            elif distance_to_closest_resource:
                self.shout(unit.id,
                           unit.move(self.intel.define_route(from_=unit.pos, to=position_closest_resource, avoid_cities=False)))

            # If we get here, there is not much we can do for the unit :(

    def command_units_gather(self):
        # TODO - Add logic to figure out to what city to deliver the resources to (based on fuel reserve + consumption and distance from this worker
        for unit in self.intel.get_workers(only_can_act=True):
            if unit.get_cargo_space_left() <= 10: # I would think 0 should work but noticed sometimes workers freeze with 96 cargo
                try:
                    # When we cannot carry any more resources, move towards a city to deliver them
                    position_closest_citytile = self.intel.calculate_shortest_distance_to(from_=[unit.pos], to=self.intel.map.city_tiles_positions[self.intel.player.team])
                    if position_closest_citytile:
                        self.shout(unit.id,
                                   unit.move(self.intel.define_route(unit.pos, position_closest_citytile[0][0])))
                    continue
                except NoCityTilesFoundError:
                    # When there are no citytiles found,
                    position_closest_empty_tile = self.intel.calculate_shortest_distance_to(from_=[unit.pos], to=self.intel.map.empty_tiles_positions)[0][0]
                    self.shout(unit.id,
                               unit.move(self.intel.define_route(unit.pos, position_closest_empty_tile)))

            # Whenever the unit is on a resource patch right now, let's just keep there
            if self.intel.position_has_resource(position=unit.pos):
                self.shout(unit.id,
                           unit.move(Constants.DIRECTIONS.CENTER))
                continue

            # If the unit is not carrying maximum amount of resources, and it not on a resource patch,
            # let's move towards the closest patch
            mineable_resource_positions = [p for p in self.intel.map.resource_tiles_positions if self.intel.position_has_resource(p)]
            if len(mineable_resource_positions) == 0:
                return
            position_closest_resource_patch = self.intel.calculate_shortest_distance_to(from_=[unit.pos], to=mineable_resource_positions)[0][0]
            self.shout(unit.id,
                       unit.move(self.intel.define_route(unit.pos, position_closest_resource_patch)))

    def command_units_expand(self):
        pass

    def command_units_build(self):
        citytiles_to_build = 2  # TODO define how many citytiles to build maximally every step

        # Define what workers will be building a citytile and where they will build it

        workers = [w for w in self.intel.get_workers(only_can_act=True) if w.has_resources_for_city()]
        if len(workers) == 0:
            # Whenever there are no workers that can act and have 100 wood, let's consider all workers having
            # 100 wood in cargo
            workers = [w for w in self.intel.get_workers(only_can_act=False) if w.has_resources_for_city()]

        locations_to_build = self.intel.get_new_positions_for_city()
        if len(locations_to_build) == 0:
            # When there are no vacant positions neighboring an existing city, allow every empty tile
            locations_to_build = self.intel.map.empty_tiles_positions

        # Order the workers in such a way that the shortest distance to travel is first
        ordered_workers = [(w, i[0], i[1]) for w, i in zip(workers, self.intel.calculate_shortest_distance_to(
            from_=[i.pos for i in workers], to=locations_to_build))]
        for worker, position, distance in sorted(ordered_workers, key=lambda x: x[2])[:citytiles_to_build]:
            if distance == 0:
                if worker.can_act():
                    self.shout(worker.id, worker.build_city())
                    self.intel.reserve_position(worker.pos)
            elif worker.can_act():
                # Todo store the action in memory so next step we know this was happening
                self.shout(worker.id,
                           worker.move(self.intel.define_route(from_=worker.pos, to=position, avoid_cities=True)))
