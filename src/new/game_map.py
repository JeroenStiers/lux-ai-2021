import logging
import math
from typing import List, Union, Tuple, Dict

from constants import Constants


class Resource:
    def __init__(self, r_type: str, amount: int):
        self.type = r_type
        self.amount = amount


class Cell:
    def __init__(self, x, y):
        self.pos = Position(x, y)
        self.resource: Union[Resource, None] = None
        self.citytile = None
        self.road = 0

    def __eq__(self, other):
        return self.pos == other.pos

    def __repr__(self):
        return f"<CELL {self.pos}, {self.resource} {self.citytile})>"

    def has_resource(self):
        return self.resource is not None and self.resource.amount > 0

    def has_city(self):
        return self.citytile is not None


class GameMap:
    def __init__(self, width, height):
        self.height = height
        self.width = width
        self.map: List[List[Cell]] = [None] * height
        for y in range(0, self.height):
            self.map[y] = [None] * width
            for x in range(0, self.width):
                self.map[y][x] = Cell(x, y)
        self.resource_tiles_positions: List[Position] = []
        self.empty_tiles_positions: List[Position] = []
        self.city_tiles_positions: List[List[Position], List[Position]] = [[], []]

    def get_cell_by_pos(self, pos) -> Cell:
        return self.map[pos.y][pos.x]

    def get_cell(self, x, y) -> Cell:
        return self.map[y][x]

    def _setResource(self, r_type, x, y, amount):
        """
        do not use this function, this is for internal tracking of state
        """
        cell = self.get_cell(x, y)
        cell.resource = Resource(r_type, amount)

    def generate_overview(self):
        """
        Fills the overview variables
        """
        for y in range(self.height):
            for x in range(self.width):
                cell = self.get_cell(x, y)
                if cell.has_resource():
                    self.resource_tiles_positions.append(cell.pos)
                elif cell.has_city():
                    self.city_tiles_positions[cell.citytile.team].append(cell.pos)
                else:
                    self.empty_tiles_positions.append(cell.pos)

    def reserve_position_for(self, position: 'Position', _type: str, player_id: int):
        if _type == 'CITYTILE':
            self.city_tiles_positions[player_id].append(position)
        else:
            raise ValueError(f'The provided value for type ({_type}) is not an allowed value')


class Position:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return f'<POSITION {self.x} {self.y}>'

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __sub__(self, pos) -> float:
        # return abs(pos.x - self.x) + abs(pos.y - self.y)
        return math.sqrt((pos.x - self.x)**2 + (pos.y - self.y)**2)

    def __key(self):
        return(self.x, self.y)

    def __hash__(self):
        return hash(self.__key())

    def distance_to(self, pos):
        """
        Returns Manhattan (L1/grid) distance to pos
        """
        return self - pos

    def is_adjacent(self, pos):
        return (self - pos) <= 1

    def equals(self, pos):
        return self == pos

    def translate(self, direction, units) -> 'Position':
        if direction == Constants.DIRECTIONS.NORTH:
            return Position(self.x, self.y - units)
        elif direction == Constants.DIRECTIONS.EAST:
            return Position(self.x + units, self.y)
        elif direction == Constants.DIRECTIONS.SOUTH:
            return Position(self.x, self.y + units)
        elif direction == Constants.DIRECTIONS.WEST:
            return Position(self.x - units, self.y)
        elif direction == Constants.DIRECTIONS.CENTER:
            return Position(self.x, self.y)

    def direction_to(self, target_pos: 'Position') -> Constants.DIRECTIONS:
        """
        Return closest direction to target_pos from this position
        """
        return self.direction_to_options(target_pos)[0][2]

    def direction_to_options(self, target_pos: 'Position') -> List[Tuple[float, 'Position', Constants.DIRECTIONS]]:
        """
        Return a list of all 4 neighboring positions ordered from smallest to biggest distance towards the target_pos
        """
        check_dirs = [
            Constants.DIRECTIONS.NORTH,
            Constants.DIRECTIONS.EAST,
            Constants.DIRECTIONS.SOUTH,
            Constants.DIRECTIONS.WEST,
        ]
        to_return: List[Tuple[float, 'Position', Constants.DIRECTIONS]] = []
        for direction in check_dirs:
            newpos = self.translate(direction, 1)
            dist = target_pos.distance_to(newpos)
            to_return.append((dist, newpos, direction))
        to_return = sorted(to_return, key=lambda x: x[0])
        return to_return

    def __str__(self) -> str:
        return f"({self.x}, {self.y})"
