import math
from enum import Enum
from typing import Tuple

from constants import Constants
from game import Game, Player


class StrategiesCitytiles(Enum):
    GROW = 'G'      # Building more units
    RESEARCH = 'R'  # Focussing on Research


class StrategiesUnits(Enum):
    BUILD = 'B'     # Building more citytiles
    GATHER = 'G'    # Focussing on gathering resources
    EXPAND = 'E'    # Expanding towards a new city
    LOCKDOWN = 'L'  # Keep the units within the city limits


class Strategist:
    """
    The strategist will analyse the initial situation and provide the Commander with a general strategy to follow. Options
    It is up to the Commander to decide how this information will impact their decisions.
    """

    def __init__(self, game: Game, player: Player, opponent: Player):
        self.game = game
        self.player = player
        self.opponent = opponent

    def strategise(self) -> Tuple[StrategiesCitytiles, StrategiesUnits]:
        return self.define_citytile_strategy(), self.define_units_strategy()

    def define_citytile_strategy(self):
        """
        Define the strategy to follow for the citytiles. Options are to focus on growth (building more workers)
        and focussing on research.
        """
        if self.player.city_tile_count - len(self.player.units) > 0 and len(self.player.units) < 10:
            # Currently we have a max of 10 units in total in other to keep the overview
            # If we have 10 units in total, we will always focus on RESEARCH
            return StrategiesCitytiles.GROW
        return StrategiesCitytiles.RESEARCH

    def define_units_strategy(self):
        """
        Define the strategy for units to follow. Options are BUILD (building more citytiles), GATHER (to gather more
        resources) and EXPAND (which means travelling towards another patch and starting a new city
        """
        # Define the reserve threshold we are looking for in each city based on how long we have until nightfall
        reserve_threshold_multiplier = 0.5 if self.game.clock.pod == Constants.POD.MORNING else 5
        # Validate for each city if it meets the reserve threshold
        city_meets_reserve_threshold = [1 for c in self.player.cities.values() if c.fuel > (c.light_upkeep * reserve_threshold_multiplier)]
        # Calculate the ratio of citytiles in comparison with the opponent.
        citytile_ratio = math.inf if self.opponent.city_tile_count == 0 else (self.player.city_tile_count * 1.0) / self.opponent.city_tile_count

        # If it is night, we will keep the units inside the cities for the night
        if self.game.clock.pod in (Constants.POD.NIGHT, Constants.POD.EVENING):
            return StrategiesUnits.LOCKDOWN

        # If all cities are meeting the resource reserve threshold
        if sum(city_meets_reserve_threshold) == len(city_meets_reserve_threshold):
            # If we don't have a significant lead, we will build more citytiles
            if self.player.city_tile_count < 5 or citytile_ratio < 1.2:
                return StrategiesUnits.BUILD

        # In all other cases, let's focus on gathering resources
        return StrategiesUnits.GATHER

