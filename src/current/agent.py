import math

from typing import List, Set
from game import Game
from game_objects import Unit
from game_map import Cell, Position
from constants import Constants

from annotate import x as draw_x
from annotate import sidetext

from strategist import Strategist
from commander import Commander

DIRECTIONS = Constants.DIRECTIONS
game_state = None
unit_move_to = []
action_to_append = []
city_tiles: List[Cell] = []
commander = Commander()


def unit_try_to_move_to(position, unit: Unit):
    global unit_move_to
    global action_to_append

    if position in unit_move_to:
        action_to_append.append(unit.move(DIRECTIONS.CENTER))
    else:
        action_to_append.append(unit.move(unit.pos.direction_to(position)))
        unit_move_to.append(position)


def unit_try_to_move_to_no_city_tile(target_pos: Position, unit: Unit):
    global unit_move_to
    global city_tiles

    options = unit.pos.direction_to_options(target_pos)

    for option in options:
        if option[1] in unit_move_to \
                or option[1] in [i.pos for i in city_tiles]:
            continue
        else:
            unit_move_to.append(option[1])
            action_to_append.append(unit.move(option[2]))


def agent(observation, configuration):
    global game_state
    global commander

    ### Do not edit ###
    if observation["step"] == 0:
        game_state = Game()
        game_state._initialize(observation["updates"])
        game_state._update(observation["updates"][2:])
        game_state.id = observation.player
    else:
        game_state._update(observation["updates"])
    
    actions = []

    ### AI Code goes down here! ###
    player = game_state.players[observation.player]
    opponent = game_state.players[(observation.player + 1) % 2]

    strategy_citytiles, strategy_units = Strategist(game_state, player, opponent).strategise()
    actions.append(sidetext(strategy_citytiles))
    actions.append(sidetext(strategy_units))

    commands_citytiles, commands_units = commander.command(game_state, player, strategy_citytiles, strategy_units)

    actions.extend(commands_citytiles)
    actions.extend(commands_units)
    return actions


    # we iterate over all our units and do something with them
    for unit in player.units:
        action_to_append = []
        action_parsed = False

        if unit.is_worker() and unit.can_act():
            logger.info(unit)
            closest_dist = math.inf
            closest_resource_tile = None

            if not cities_need_fuel and unit.cargo.wood == 100:

                # When no new possible city tile is found, we just pick the closest empty tile
                if len(possible_new_city_tiles) == 0:
                    possible_new_city_tiles = set(empty_tiles.copy())

                logger.debug(list(possible_new_city_tiles))
                # Find the closest empty Cell, go to it and build a cityTile on it
                closest_dist = math.inf
                closest_empty_tile = None
                for empty_tile in list(possible_new_city_tiles):
                    if not empty_tile.has_city():
                        dist = empty_tile.pos.distance_to(unit.pos)
                        if dist == 0:
                            # If the unit is at an empty cell, build a citytile
                            logger.info("Building a city")
                            action_to_append.append(unit.build_city())
                            action_parsed = True
                            break
                        if dist < closest_dist:
                            closest_dist = dist
                            closest_empty_tile = empty_tile
                if closest_empty_tile is not None and not action_parsed:
                    logger.info("Move to empty tile to build city")
                    action_to_append.append(draw_x(closest_empty_tile.pos.x, closest_empty_tile.pos.y))
                    unit_try_to_move_to_no_city_tile(closest_empty_tile.pos, unit)

            elif unit.get_cargo_space_left() > 20:
                # if the unit is a worker and we have space in cargo, lets find the nearest resource tile and try to mine it
                for resource_tile in resource_tiles:
                    if resource_tile.resource.type == Constants.RESOURCE_TYPES.COAL and not player.researched_coal(): continue
                    if resource_tile.resource.type == Constants.RESOURCE_TYPES.URANIUM and not player.researched_uranium(): continue
                    dist = resource_tile.pos.distance_to(unit.pos)
                    if dist < closest_dist:
                        closest_dist = dist
                        closest_resource_tile = resource_tile

                if (
                    # We move to the closest resource tile if we are not already at it and it is not getting dark out there.
                    closest_resource_tile is not None
                    and closest_dist > 1
                    and game_state.clock.pod not in [Constants.POD.EVENING, Constants.POD.NIGHT]
                ) or (
                    # If we can build a new city and the closest resource tile is adjacent to the current position and
                    # we are standing on a city tile, let's move to the resource tile so we don't automatically drop
                    # drop all resources into the city
                    not cities_need_fuel
                    and unit.pos in [i.pos for i in city_tiles]
                ):
                    logger.info("Move to closest resource")
                    unit_try_to_move_to(closest_resource_tile.pos, unit)

            else:
                # if unit is a worker and there is no cargo space left, and we have cities, lets return to them
                if len(player.cities) > 0:
                    amount_of_fuel = math.inf
                    closest_city_tile = None
                    # Move to the closest city tile of the city having least amount of fuel
                    logger.debug(f"Testing all cities to see which has got less fuel")
                    for city in player.cities.values():
                        logger.debug(f"{city.cityid} - FUEL: {city.fuel} - AMOUNT_OF_FUEL: {amount_of_fuel}")
                        if city.fuel > amount_of_fuel:
                            logger.debug("CONTINUE")
                            continue
                        amount_of_fuel = city.fuel
                        closest_dist = math.inf
                        for city_tile in city.citytiles:
                            dist = city_tile.pos.distance_to(unit.pos)
                            if dist < closest_dist:
                                closest_dist = dist
                                closest_city_tile = city_tile
                            logger.debug(f"Closest city tile: {closest_city_tile}")
                    if closest_city_tile is not None:
                        logger.info("Move to cityTile to drop off fuel")
                        unit_try_to_move_to(closest_city_tile.pos, unit)

        if len(action_to_append) != 1:
            logger.warning(f"The unit {unit} has got {len(action_to_append)} actions planned: {action_to_append}")
        else:
            actions.extend(action_to_append)

