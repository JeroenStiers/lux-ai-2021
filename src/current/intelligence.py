from typing import Tuple, List, Set

from constants import Constants
from game import Player, GameMap, Clock, Game
from game_map import Position
from game_objects import Unit, CityTile
from errors import NoCityTilesFoundError


class Intelligence:
    """
    The Intelligence department is responsible for providing the Commander with (spatial) information / insights so
    that they can make the best decisions.
    """

    def __init__(self):
        self.map: GameMap
        self.clock: Clock
        self.player: Player
        self.workers: List[Unit]
        self.carts: List[Unit]
        self.reserved_positions: List[Position]

    def brief(self, game: Game, player: Player):
        """
        Get the new information from the commander to update the stored information
        """
        self.map = game.map
        self.clock = game.clock
        self.player = player
        self.reserved_positions = []
        self.workers = [i for i in self.player.units if i.is_worker()]
        self.carts = [i for i in self.player.units if i.is_cart()]

    def get_citytiles(self, only_can_act: bool = False) -> List[CityTile]:
        to_return: List[CityTile] = []
        for city in self.player.cities.values():
            for citytile in city.citytiles:
                if (only_can_act and citytile.can_act()) or not only_can_act:
                    to_return.append(citytile)
        return to_return

    def get_workers(self, only_can_act: bool = False) -> List[Unit]:
        to_return: List[Unit] = []
        for unit in self.workers:
            if (only_can_act and unit.can_act()) or not only_can_act:
                to_return.append(unit)
        return to_return

    def get_new_positions_for_city(self) -> List[Position]:
        """
        Define Positions that are empty and located next to an already existing citytile
        """
        to_return: Set[Position] = set()
        for empty_cell in self.map.empty_tiles_positions:
            for d in [Constants.DIRECTIONS.NORTH, Constants.DIRECTIONS.EAST, Constants.DIRECTIONS.SOUTH,
                      Constants.DIRECTIONS.WEST]:
                adjacent_tile = empty_cell.translate(direction=d, units=1)
                if adjacent_tile in self.map.city_tiles_positions[self.player.team]:
                    to_return.add(empty_cell)
        return list(to_return)

    @staticmethod
    def calculate_shortest_distance_to(from_: List[Position], to: List[Position]) -> List[Tuple[Position, float]]:
        """
        Calculates for each of the elements in 'from_' the distance to each element in 'to' and returns
        for each 'from_' element the 'to' position that is closest located to it
        """

        try:
            to_return: List[Tuple[Position, float]] = []
            for el_from in from_:
                tmp = sorted([(i, el_from.distance_to(i)) for i in to], key=lambda x: x[1])
                to_return.append((tmp[0]))
            return to_return
        except IndexError:
            return False

    def define_route(self, from_: Position, to: Position, avoid_cities: bool = False):
        """
        Defines the move command for this unit in order to get from 'from_' to 'to'.
        If avoid_cities is True, the returned command ensurse no citytile is crossed.
        """
        current_distance = from_.distance_to(to)
        options = from_.direction_to_options(to)
        for distance, position, direction in options:

            # [(1, < POSITION 9 2 >, 's'), (3, < POSITION 9 0 >, 'n'), (3, < POSITION 10 1 >, 'e'), (3, < POSITION 8 1 >, 'w')]

            if avoid_cities and position in self.map.city_tiles_positions[self.player.team]:
                # If we should avoid citytiles and this position contains a city, move on to the next option
                continue

            if not avoid_cities and distance > current_distance:
                # If we would move in the 'wrong' direction, we chose to not move at all if we are not
                # limited by city_tiles.
                return Constants.DIRECTIONS.CENTER

            if position not in self.reserved_positions:
                # If the current suggested position is not yet reserved, this unit can move to the suggested position
                #
                # If this new position is not a citytile, we add it to the reserved_positions so that no other unit
                # will move towards it
                if position not in self.map.city_tiles_positions[self.player.team]:
                    self.reserved_positions.append(position)
                return direction

    def reserve_position(self, position: Position):
        self.reserved_positions.append(position)

    def position_has_resource(self, position: Position) -> bool:
        """
        Checks if the provided position has got a mineable resource.
        """
        cell = self.map.get_cell_by_pos(position)
        if cell.has_resource():
            if (cell.resource.type == Constants.RESOURCE_TYPES.COAL and self.player.researched_coal()) or (
                    cell.resource.type == Constants.RESOURCE_TYPES.URANIUM and self.player.researched_uranium()) or \
                    cell.resource.type == Constants.RESOURCE_TYPES.WOOD:
                return True
        return False
